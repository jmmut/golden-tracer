# Golden tracer

This project shows some experiments on the problem of sampling with uniform and
increasing density.

[video here](https://www.youtube.com/watch?v=pUWPxvCgo2g)


## Build requirements

Linux:

```
git clone https://bitbucket.org/mutcoll/golden-tracer.git
cd golden-tracer
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/golden-tracer
```

Windows:

```
git clone https://bitbucket.org/mutcoll/golden-tracer.git
cd golden-tracer
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/golden-tracer.exe
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)
