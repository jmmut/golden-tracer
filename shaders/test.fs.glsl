varying vec2 v_texCoord;
varying float v_attrib;
uniform int t;

/**
 * here I'm using time to change the color and the vertex myAttribute to set alpha
 */
void main()
{
	float colorSpeed = 0.01;
	float t_f = float(t) * 0.02;

	gl_FragColor = vec4(
			(cos(colorSpeed*v_attrib + t_f + 0.0)+1.0)*0.5,
			(cos(colorSpeed*v_attrib + t_f + 2.0)+1.0)*0.5,
			(cos(colorSpeed*v_attrib + t_f + 4.0)+1.0)*0.5,
			1.0);
}

