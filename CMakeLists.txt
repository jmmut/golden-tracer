project(GoldenTracer)
cmake_minimum_required(VERSION 2.8)


if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)# Not CLion
else()
    include(${CMAKE_HOME_DIRECTORY}/conanbuildinfo.cmake) #Clion, with conanbuildinfo.cmake in root folder
endif()
conan_basic_setup()

include_directories(src)
include_directories(include)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
include_directories(src)
include_directories(include)

set(SOURCE_FILES
        src/Manager.cpp src/Manager.h
        )

set(PROGRAM_LIBRARY goldenTracerLib)
add_library(${PROGRAM_LIBRARY} ${SOURCE_FILES})
target_link_libraries(${PROGRAM_LIBRARY} ${CONAN_LIBS})

set(EXECUTABLE_NAME golden-tracer)
add_executable(${EXECUTABLE_NAME} src/main.cpp )
target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})

