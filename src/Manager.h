/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_MANAGER_H
#define MYTEMPLATEPROJECT_MANAGER_H

#include <random>
#include <chrono>
#include <complex>
#include "events/Dispatcher.h"
#include "events/EventCallThis.h"
#include "manager_templates/FlyerManagerBase.h"

class MyWindow : public WindowRenderer {
public:
    explicit MyWindow(Uint32 flags = 0) : WindowRenderer{flags} {}
    ~MyWindow() override = default;

    void open() override;
    void drawRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect);
    void getWindowSize(int &w, int &h);
    void renderClearBlack();
    void setTitle(const std::string &title);

    virtual void resize(Uint32 w, Uint32 h) override;

private:
//    SDL_Texture *texture = nullptr;
};

class Manager: public MediumManagerBase {
public:
    Manager( Dispatcher &d, std::shared_ptr< MyWindow> w);

protected:
    void onDisplay( Uint32 ms) override;
    void onKeyChange(SDL_KeyboardEvent &event) override;

private:
    MyWindow &tryGetWindow();
    std::weak_ptr<MyWindow> mywindow;
    int iteration = 0;
    int pixelWidth = 5;
    std::string title;

    std::random_device randomDevice;  //Will be used to obtain a seed for the random number engine
    std::mt19937 randomGenerator; //Standard mersenne_twister_engine seeded with randomDevice()
    std::uniform_int_distribution<> uniformDistribution;

    std::chrono::nanoseconds phiNs;
    std::chrono::nanoseconds phiSquaredInvertedNs;
    std::chrono::nanoseconds randomNs;
    int mode = 1;

    std::complex<double> complexIteration;
    std::complex<double> complexMovingIteration;

    std::stringstream &writeNanoseconds(std::stringstream &ss, const std::chrono::nanoseconds &nanoseconds) const;
    void drawBands(MyWindow &window);
    void drawSurfaces(MyWindow &window);
};

#endif //MYTEMPLATEPROJECT_MANAGER_H
