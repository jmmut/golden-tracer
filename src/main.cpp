
#include "Manager.h"

int main(){
    Dispatcher dispatcher;
    dispatcher.addEventHandler(
            [&dispatcher](SDL_Event &event) {
                dispatcher.endDispatcher();
            },
            [](SDL_Event &event) {
                return event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE);
            }
    );

    auto window = std::make_shared<MyWindow>();
    window->open();
    dispatcher.addEventHandler(
            [&window] (SDL_Event &event){
                window->close();
            },
            [windowID = window->getWindowID()] (SDL_Event &event){
                return event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE;
            }
    );

    Manager manager( dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();

    exit(0);
    return 0;
}

