/**
 * @file Manager.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include <algorithm>
#include <complex>
#include "Manager.h"
#include "utils/exception/StackTracedException.h"
#include "utils/time/TimeRecorder.h"

using randomize::utils::exception::StackTracedException;
using randomize::utils::time::TimeRecorder;
using namespace std::string_literals;
using namespace std::chrono_literals;

const double PHI = (1 + sqrt(5.0)) / 2.0;
const double ONE_DIVIDED_BY_PHI = 1.0 / PHI;
const double ONE_DIVIDED_BY_PHI_SQUARED = ONE_DIVIDED_BY_PHI * ONE_DIVIDED_BY_PHI;
const double ONE_DIVIDED_BY_PHI_CUBED = ONE_DIVIDED_BY_PHI_SQUARED * ONE_DIVIDED_BY_PHI;
const auto PHI_ANGLE = 2 * M_PI * ONE_DIVIDED_BY_PHI;
const auto PHI_REAL = cos(PHI_ANGLE);
const auto PHI_IMAG = sin(PHI_ANGLE);
const auto PHI_TURNING = std::complex<double>{PHI_REAL, PHI_IMAG};

const auto UNKNOWN_NUMBER = sqrt(2.0) - 1;
const auto UNKNOWN_NUMBER_2 = (sqrt(PHI*PHI + 4) - PHI)/2;

void MyWindow::open() {
    WindowRenderer::open();
    if (renderer == nullptr) {
        throw StackTracedException("renderer was null. inner message: "s + SDL_GetError());
    }
//    texture = createWindowTexture();
//    if (texture == nullptr) {
//        throw StackTracedException("texture was null. inner message: "s + SDL_GetError());
//    }
    SDL_SetRenderTarget(renderer, NULL);
    renderClearBlack();
}

void MyWindow::drawRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect) {
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
//    SDL_RenderDrawRect(renderer, &rect);
    SDL_RenderFillRect(renderer, &rect);
}

void MyWindow::getWindowSize(int &w, int &h) {
    SDL_GetWindowSize(window, &w, &h);
}

void MyWindow::renderClearBlack() {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    WindowRenderer::renderClear();
}

void MyWindow::setTitle(const std::string &title) {
    SDL_SetWindowTitle(window, title.c_str());
}

void MyWindow::resize(Uint32 w, Uint32 h) {
    Window::resize(w, h);
    renderClearBlack();
}

Manager::Manager(Dispatcher &d, std::shared_ptr<MyWindow> w)
        : MediumManagerBase( d, w),
        mywindow{w},
        randomDevice{},
        randomGenerator{randomDevice()},
        uniformDistribution{0, 1},
        phiNs{0ns},
        phiSquaredInvertedNs{0ns},
        randomNs{0ns},
        complexIteration{1, 0},
        complexMovingIteration{1, 0}
        {
    setDisplayFPS(10);
}

void Manager::onDisplay(Uint32 ms) {
    auto &window = tryGetWindow();
//    window.renderClearBlack();

    int normalizedMode = mode % 2;
    if (normalizedMode == 0) {
        drawBands(window);
    } else {
        drawSurfaces(window);
    }

    window.renderPresent();
}

void Manager::drawBands(MyWindow &window) {
    int width = 1, height = 1;
    window.getWindowSize(width, height);

    TimeRecorder time;

    SDL_Rect rect{0, 0, pixelWidth, 50};

    rect.y = 150;
    time.start();
    rect.x = std::llround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * width) % width;
    time.stop();
    phiSquaredInvertedNs += time.nanoseconds();
    window.drawRect(80, 120, 40, 255, rect);

    rect.y = 250;
    time.start();
    std::uniform_int_distribution<>::param_type distributionParameters{0, width};
    rect.x = uniformDistribution(randomGenerator, distributionParameters);
    time.stop();
    randomNs += time.nanoseconds();
    window.drawRect(120, 40, 80, 255, rect);

    rect.y = 50;
    time.start();
    rect.x = std::llround(PHI * iteration * width) % width;
    time.stop();
    phiNs += time.nanoseconds();
    window.drawRect(40, 80, 120, 255, rect);

    iteration++;

    std::stringstream ss;
    ss << "window width: " << width << ". number of lines: " << iteration << ". FPS: " << getDisplayFPS()
            << ". line width: " << pixelWidth << ". accumulated times: ";

    writeNanoseconds(ss, phiNs);
    writeNanoseconds(ss, phiSquaredInvertedNs);
    writeNanoseconds(ss, randomNs);

    title = ss.str();
    window.setTitle(title);
}

std::stringstream &Manager::writeNanoseconds(std::stringstream &ss, const std::chrono::nanoseconds &nanoseconds) const {
    ss << "0.";
    ss.fill('0');
    ss.width(9);
    ss << nanoseconds.count() << " s ";
    return ss;
}


long long int inRangeSlide(int inclusiveMax, double value) {
    auto intervalSize = inclusiveMax + 1;
    auto remainder = std::fmod(value, intervalSize);
    auto positiveRemainder = (remainder + intervalSize) * 0.5;
    return positiveRemainder;
}
long long int inRange(int inclusiveMax, double value) {
    auto intervalSize = inclusiveMax + 1;
    auto remainder = std::fmod(value, intervalSize);
    auto positiveRemainder = std::fmod(remainder + intervalSize, intervalSize);
    return positiveRemainder;
}
long long int inRange(int inclusiveMax, long long int value) {
    auto intervalSize = inclusiveMax + 1;
    auto remainder = value % intervalSize;
    remainder += intervalSize * 0.5;
    return remainder;
}

void Manager::drawSurfaces(MyWindow &window) {

    int width = 1, height = 1;
    window.getWindowSize(width, height);

    TimeRecorder time;
    int horizontalDivisions = 4;
    int verticalDivisions = 3;
    int subSurfaceWidth = width * 0.8 / horizontalDivisions;
    int subSurfaceHeight = height * 0.8 / verticalDivisions;
    auto singlePadX = width * 0.1 / horizontalDivisions;
    auto singlePadY = height * 0.1 / verticalDivisions;

    SDL_Rect rect{0, 0, pixelWidth, pixelWidth};

    int padX = std::lround(singlePadX + 0 * width / horizontalDivisions);
    int padY = std::lround(singlePadY + 0 * height / verticalDivisions);
    std::uniform_int_distribution<>::param_type distributionParametersWidth{0, subSurfaceWidth};
    rect.x = padX + uniformDistribution(randomGenerator, distributionParametersWidth);
    std::uniform_int_distribution<>::param_type distributionParametersHeight{0, subSurfaceHeight};
    rect.y = padY + uniformDistribution(randomGenerator, distributionParametersHeight);
    window.drawRect(120, 40, 80, 255, rect);


    padX = std::lround(singlePadX + 1 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 0 * height / verticalDivisions);
    int area = subSurfaceWidth * subSurfaceHeight;
    long pointIndex = std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * area) % area;
    rect.x = padX + pointIndex % subSurfaceWidth;
    rect.y = padY + (pointIndex / subSurfaceWidth) % subSurfaceHeight;
    window.drawRect(40, 80, 120, 255, rect);



    padX = std::lround(singlePadX + 0 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 1 * height / verticalDivisions);
    area = subSurfaceWidth * subSurfaceHeight;
    pointIndex = std::lround(ONE_DIVIDED_BY_PHI_SQUARED * 2 * iteration * area) % area;
    rect.x = padX + pointIndex % subSurfaceWidth;
    pointIndex = std::lround(ONE_DIVIDED_BY_PHI_SQUARED * 2 * iteration * area + 1) % area;
    rect.y = padY + (pointIndex / subSurfaceWidth) % subSurfaceHeight;
    window.drawRect(80, 120, 40, 255, rect);


    padX = std::lround(singlePadX + 1 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 1 * height / verticalDivisions);
    area = subSurfaceWidth * subSurfaceHeight;
    rect.x = padX + std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * subSurfaceWidth) % subSurfaceWidth;
    rect.y = padY + iteration%subSurfaceHeight;
    window.drawRect(80, 40, 120, 255, rect);


    padX = std::lround(singlePadX + 0 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 2 * height / verticalDivisions);
    area = subSurfaceWidth * subSurfaceHeight;
    pointIndex = std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * area) % area;
    rect.x = padX + std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * subSurfaceWidth + pointIndex) % subSurfaceWidth;
    rect.y = padY + (iteration + pointIndex / subSurfaceWidth)%subSurfaceHeight;
    window.drawRect(40, 120, 80, 255, rect);


    padX = std::lround(singlePadX + 1 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 2 * height / verticalDivisions);
    area = subSurfaceWidth * subSurfaceHeight;
    pointIndex = std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * area) % area;
    rect.x = padX + (pointIndex % subSurfaceWidth + std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * subSurfaceWidth) % subSurfaceWidth)/2;
    rect.y = padY + ((pointIndex / subSurfaceWidth) % subSurfaceHeight + iteration%subSurfaceHeight)/2;
    rect.x = padX + (uniformDistribution(randomGenerator, distributionParametersWidth) + uniformDistribution(randomGenerator, distributionParametersWidth))/2;
    rect.y = padY + (uniformDistribution(randomGenerator, distributionParametersHeight) + uniformDistribution(randomGenerator, distributionParametersHeight))/2;
    window.drawRect(120, 80, 40, 255, rect);


    padX = std::lround(singlePadX + 2 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 0 * height / verticalDivisions);
    auto previousComplexIteration = complexIteration;
    complexIteration = complexIteration * PHI_TURNING;
//    auto modulus = ONE_DIVIDED_BY_PHI_SQUARED * iteration;
    auto modulus = 0.01 * iteration;
    rect.x = padX + inRangeSlide(subSurfaceWidth, complexIteration.real()
            * subSurfaceWidth
            * modulus
    );
    rect.y = padY + inRangeSlide(subSurfaceHeight, complexIteration.imag()
            * subSurfaceHeight
            * modulus
    );
    window.drawRect(120, 100, 20, 255, rect);

    padX = std::lround(singlePadX + 2 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 1 * height / verticalDivisions);
    modulus = ONE_DIVIDED_BY_PHI_SQUARED * iteration;
    auto modifiedComplexIteration = complexIteration * std::fmod(modulus, 1.0);
    rect.x = padX + inRangeSlide(subSurfaceWidth, modifiedComplexIteration.real()
            * subSurfaceWidth
    );
    rect.y = padY + inRangeSlide(subSurfaceHeight, modifiedComplexIteration.imag()
            * subSurfaceHeight
    );
    window.drawRect(100, 20, 120, 255, rect);

    padX = std::lround(singlePadX + 2 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 2 * height / verticalDivisions);
    modifiedComplexIteration = (complexIteration + std::complex<double>{1.0, 1.0}) * 0.5;
    rect.x = padX + inRange(subSurfaceWidth, modifiedComplexIteration.real()
            * subSurfaceWidth
    );
    rect.y = padY + inRange(subSurfaceHeight, modifiedComplexIteration.imag()
            * subSurfaceHeight
    );
    window.drawRect(20, 120, 100, 255, rect);


    padX = std::lround(singlePadX + 3 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 0 * height / verticalDivisions);
//    modulus = ONE_DIVIDED_BY_PHI_SQUARED * iteration;
//    modifiedComplexIteration = (complexIteration * std::fmod(modulus, 1.0) + std::complex<double>{1.0, 1.0}) * 0.5;
//    rect.x = padX + inRange(subSurfaceWidth, modifiedComplexIteration.real()
//            * subSurfaceWidth
//    );
//    rect.y = padY + inRange(subSurfaceHeight, modifiedComplexIteration.imag()
//            * subSurfaceHeight
//    );
    area = subSurfaceWidth * subSurfaceHeight;
    auto complexDirection = std::complex<double>{ONE_DIVIDED_BY_PHI_SQUARED, UNKNOWN_NUMBER} * ONE_DIVIDED_BY_PHI_SQUARED;
    auto complexPoint = complexDirection * (double)iteration;
    rect.x = padX + std::lround(complexPoint.real() * subSurfaceWidth) % subSurfaceWidth;
    rect.y = padY + std::lround(complexPoint.imag() * subSurfaceHeight) % subSurfaceHeight;
    window.drawRect(20, 100, 120, 255, rect);


    padX = std::lround(singlePadX + 3 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 1 * height / verticalDivisions);
//    modifiedComplexIteration = complexMovingIteration;
//    rect.x = padX + inRangeSlide(subSurfaceWidth, modifiedComplexIteration.real()
//            * subSurfaceWidth
//    );
//    rect.y = padY + inRangeSlide(subSurfaceHeight, modifiedComplexIteration.imag()
//            * subSurfaceHeight
//    );
    auto complexDirection2 = std::complex<double>{ONE_DIVIDED_BY_PHI_SQUARED, UNKNOWN_NUMBER_2} * UNKNOWN_NUMBER_2;
    auto complexPoint2 = complexDirection2 * (double)iteration;
    rect.x = padX + std::lround(complexPoint2.real() * subSurfaceWidth) % subSurfaceWidth;
    rect.y = padY + std::lround(complexPoint2.imag() * subSurfaceHeight) % subSurfaceHeight;
    window.drawRect(120, 20, 100, 255, rect);


    padX = std::lround(singlePadX + 3 * width / horizontalDivisions);
    padY = std::lround(singlePadY + 2 * height / verticalDivisions);
    area = subSurfaceWidth * subSurfaceHeight;
    rect.x = padX + std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration * subSurfaceWidth) % subSurfaceWidth;
    rect.y = padY + std::lround(ONE_DIVIDED_BY_PHI_SQUARED * iteration) % subSurfaceHeight;
    window.drawRect(100, 120, 20, 255, rect);

    complexIteration = complexIteration * PHI_TURNING;
    complexMovingIteration = complexMovingIteration + complexIteration;
    iteration++;
    std::stringstream ss;
    ss << "surface size (x, y): " << subSurfaceWidth << ", " << subSurfaceHeight << ". number of points: " << iteration
            << ". FPS: " << getDisplayFPS() << ". line width: " << pixelWidth
            << ". points: " << (float)iteration/area * 100 << "%"
//            << ". accumulated times: "
 ;

//    writeNanoseconds(ss, phiNs);
//    writeNanoseconds(ss, phiSquaredInvertedNs);
//    writeNanoseconds(ss, randomNs);
//
    title = ss.str();
    window.setTitle(title);
}

void Manager::onKeyChange(SDL_KeyboardEvent &event) {
    MediumManagerBase::onKeyChange(event);
    if (event.type == SDL_KEYUP) {
        return;
    }
    switch (event.keysym.sym) {
    case SDLK_SPACE:
        if (displayActive) {
            stopOnDisplay();
        } else {
            startOnDisplay();
        }
        break;
    case SDLK_PLUS:
    case SDLK_KP_PLUS:
        setDisplayFPS(getDisplayFPS() + 1);
        break;
    case SDLK_MINUS:
    case SDLK_KP_MINUS:
        setDisplayFPS(std::max(getDisplayFPS() - 1, 1u));
        break;
    case SDLK_ASTERISK:
    case SDLK_KP_MULTIPLY:
        tryGetWindow().renderClearBlack();
        pixelWidth++;
        iteration = 0;
        complexIteration = {0, 1};
        complexMovingIteration = {0, 1};
        break;
    case SDLK_SLASH:
    case SDLK_KP_DIVIDE:
        tryGetWindow().renderClearBlack();
        iteration = 0;
        complexIteration = {0, 1};
        complexMovingIteration = {0, 1};
        pixelWidth = std::max(pixelWidth -1, 1);
        break;
    case SDLK_c:
        tryGetWindow().renderClearBlack();
        iteration = 0;
        complexIteration = {0, 1};
        complexMovingIteration = {0, 1};
        break;
    case SDLK_m:
        tryGetWindow().renderClearBlack();
        iteration = 0;
        complexIteration = {0, 1};
        complexMovingIteration = {0, 1};
        mode++;
        break;
    default:
        break;
    }
}

MyWindow &Manager::tryGetWindow() {
    auto ptrWindow = this->mywindow.lock();
    if (ptrWindow == nullptr) {
        throw StackTracedException("window is unavailable. hint: was it freed too soon?");
    }
    return ptrWindow.operator*();
}
